package com.codingcrayons.aspectfaces.tutorial.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.codingcrayons.aspectfaces.tutorial.model.Task;

@RequestScoped
@ManagedBean(name = "taskBean")
public class TaskBean {

	/** task manager */
	@ManagedProperty("#{tasksBean}")
	private TasksBean manager;

	/** current edited task */
	private Task task = new Task();

	public Task getTask() {
		return task;
	}

	public String save() {
		// insert new task into collection
		return manager.newTask(task);
	}

	public void setManager(TasksBean manager) {
		this.manager = manager;
	}
}
