package com.codingcrayons.aspectfaces.tutorial.bean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.codingcrayons.aspectfaces.tutorial.model.Task;

@ApplicationScoped
@ManagedBean(name = "tasksBean")
public class TasksBean {

	/** All incomplete tasks */
	public final List<Task> tasks = new LinkedList<Task>();

	// JBoss AS reflection "feature". It is not able to perform invocation of isEmpty via reflection
	// due to Security manager and that the SynchronizedList is not public class. For servers
	// without Security manager such as Tomcat you are free to enable the synchronization list
	// public final List<Task> tasks = Collections.synchronizedList(new LinkedList<Task>());

	/** tasks sequence */
	private AtomicInteger taskCounter = new AtomicInteger(1);

	public List<Task> getTasks() {

		return tasks;
	}

	public String newTask(Task task) {

		if (tasks.size() >= 20) {
			// limit maximal amount of tasks
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("There can be 20 tasks at most"));
			return null;
		} else {
			// set task id
			task.setIdentifier(Long.valueOf(taskCounter.getAndIncrement()));
			// add into collection
			tasks.add(task);
			// redirect to tasks overview
			return "tasks";
		}
	}

	public void remove(Task task) {
		// remove task from collection of incomplete tasks
		tasks.remove(task);
	}

	public Task getByIdentifier(Long identifier) {
		// look up task with given ID
		for (Task task : tasks) {
			if (task.getIdentifier().equals(identifier)) return task;
		}
		return null;
	}
}
