package com.codingcrayons.aspectfaces.tutorial.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import java.lang.reflect.Method;

import com.codingcrayons.aspectfaces.util.Strings;

@FacesConverter("entityConverter")
public class EntityConverter implements Converter {

	private static final String NULL_ENTITY = "null";

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		// no object, empty reference
		if (value == null || NULL_ENTITY.equals(value)) return null;

		String entity = value.split("-")[0];
		Long identifier = Long.parseLong(value.split("-")[1]);

		try {

			// resolve data type to model provider
			Object dataProvider = context.getApplication().evaluateExpressionGet(context, "#{" + Strings.lowerFirstLetter(entity) + "sBean" + "}", Object.class);

			// model getter
			Method getter = dataProvider.getClass().getMethod("getByIdentifier", Long.class);

			// resolve to instance and return
			return getter.invoke(dataProvider, identifier);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		if (value == null) return NULL_ENTITY;
		else if (value instanceof String) return value.toString();
		else throw new UnsupportedOperationException("Conversion of type " + value.getClass() + " is not supported.");
	}
}
