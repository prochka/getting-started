package com.codingcrayons.aspectfaces.tutorial.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.util.HashSet;
import java.util.Set;

import com.codingcrayons.aspectfaces.annotations.UiOrder;

/** <p>Tasks to do with a responsible person.</p> */
public class Task {

	private Long identifier;

	private String name;

	private String responsible;

	private String email;

	private Integer priority;

	private Boolean done;

	@ManyToOne(optional = true)
	private Task parent;

	@OneToMany
	private Set<Task> children = new HashSet<Task>();

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

    @NotBlank
	@UiOrder(3)
    @Length(min = 6)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    @NotBlank
    @UiOrder(4)
    @Length(min = 6)
	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

    @NotNull
    @UiOrder(1)
	public Boolean getDone() {
		return done;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

    @Email
    @NotBlank
	@UiOrder(5)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    @Min(1)
    @Max(5)
    @NotNull
	@UiOrder(2)
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Set<Task> getChildren() {
		return children;
	}

	public Task getParent() {
		return parent;
	}

	public void setParent(Task parent) {
		this.parent = parent;
		if (parent != null) parent.children.add(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Task)) return false;

		Task task = (Task) o;

		if (done != null ? !done.equals(task.done) : task.done != null) return false;
		if (email != null ? !email.equals(task.email) : task.email != null) return false;
		if (identifier != null ? !identifier.equals(task.identifier) : task.identifier != null) return false;
		if (name != null ? !name.equals(task.name) : task.name != null) return false;
		if (parent != null ? !parent.equals(task.parent) : task.parent != null) return false;
		if (priority != null ? !priority.equals(task.priority) : task.priority != null) return false;
		if (responsible != null ? !responsible.equals(task.responsible) : task.responsible != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return identifier != null ? identifier.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "Task{" +
			"name='" + name + '\'' +
			'}';
	}
}
